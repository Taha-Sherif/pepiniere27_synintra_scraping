# README

## Table of Contents

* [About the Project](#About-the-Project)
* [Getting Started](#Getting-Started)
  * [Prerequisites](#Prerequisites)
  * [Installation](#Installation)
* [Usage](#Usage)
* [Who do I talk to?](#Who_do_I_talk_to?)

## About the Project
In this project, we worked on scraping "https://pepiniere27.synintra.com/" website.  
Our team needed the information of each company also the information of its employees.
For this purpose, we used Selenium and Beautifulsoup together so we can easily scrape such a "DYNAMIC" website.

## Getting Started

To get a local copy up and running follow these simple example steps.  

#### Prerequisites
```
pip               20.2.2
selenium          3.141.0
beautifulsoup4    4.9.1
chromeDriver      85.0.4183.87
```
#### Installation  

**1- Clone the repo** 
```
git clone git@bitbucket.org:Taha-Sherif/pepiniere27_synintra_scraping.git
```

**2- Installing pip for Python 3**  
  
Ubuntu 20.04 ships with Python 3, as the default Python installation. Complete the following steps to install pip (pip3) for Python 3:  

2.1- Start by updating the package list using the following command:
```
sudo apt update
```
2.2- Use the following command to install pip for Python 3:
```
sudo apt install python3-pip
```
2.3 Once the installation is complete, verify the installation by checking the pip version:
```
pip3 --version
```

**3- Installing Selenium**  

After installing pip succesfully, we'll use it to install selenium using this command
```
pip install selenium
```

3.2 Enter the smartocr directory for your Selenium files:

**4- Installing Beautifulsoup**
```
pip install beautifulsoup4
```

**5- Chromedriver**

Each google chrome version has its own chromedriver.  
What we recommend to do here as a first step is to check your google chrome version by typing this command  
```
google-chrome --version
```
Depending on your google chrome version you can download the right chomedriver from this link :  
https://chromedriver.chromium.org/downloads  

After downloading the chromedriver, unzip the file and put the chromedriver in the project directory (pepeniere27_synintra_scraping)

## Usage
After setting up you **virtual envirement** and installing all the needed libraries, open your work directory and execute in terminal : "python scraping.py"

## Who do I talk to?
For any additional details about the project, you can contact **Taha Sherif** by sending an e-mail to **taha.sherif@breakpoint-technology.fr**.