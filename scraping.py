from selenium import webdriver
from bs4 import BeautifulSoup
from pathlib import Path
import time
from selenium.webdriver.common.action_chains import ActionChains
import csv


def browse(url):
    #getting chromedriver path
    script_dir = Path(__file__).parent
    path=(script_dir / 'chromedriver').resolve()
    #connect chromedriver
    driver = webdriver.Chrome(path)
    # getting the website
    driver.get(url)
    driver.implicitly_wait(10) 
    return driver 

def connect(driver,id,psw):
    driver.find_element_by_id('edit-name').send_keys(id)
    driver.find_element_by_id('edit-pass').send_keys(psw)
    driver.find_element_by_id('edit-submit').click()
    driver.implicitly_wait(10)

def get_local_network(driver):
    # search and click on "Annuaire des sociétés"
    driver.find_element_by_xpath('//*[@id="block-menu-gauche-outils"]/div/ul/li[8]/a').click()
    driver.implicitly_wait(20)
    time.sleep(5)
    # search and click on pepeniere 27
    pepeniere27 = driver.find_element_by_xpath('//*[@id="wrapper-select-host"]/dl/dt/a')
    ActionChains(driver).move_to_element(pepeniere27).click().perform()
    driver.implicitly_wait(10)
    # search and click on reseau local 
    reseau_local = driver.find_element_by_xpath('//*[@id="dropdown-select-host"]/li[2]/a/span')
    ActionChains(driver).move_to_element(reseau_local).click().perform()
    driver.implicitly_wait(10)
    time.sleep(3)

#load page (get more companies)
def more_load(driver):
    load = True
    nb = 0
    while (load== True):
        try:
            driver.find_element_by_id('more-load').click()
            driver.implicitly_wait(10)
            nb += 1
            time.sleep(3)
        except:
            load = False
    print('No more companies to load')
    print('We loaded the page',nb,'times')
    driver.implicitly_wait(5)
    time.sleep(5)

######################################################################
######################## FOR COMPANIES ###############################

#get companies urls
def get_companies_urls(driver):

    html = driver.page_source
    soup = BeautifulSoup(html,'html.parser')
    # find all companies url
    companies_list = soup.find_all("a", class_ ='societe')
    companies_urls =[]
    #Now, extract the companies sources(urls) from companies_list
    for company in companies_list:
        try:
            companies_urls.append(company["href"]) #source address of an img 
        except KeyError:
            print('no url provided')
            
    return companies_urls


# return a list that contains all the company name, email, phone number, collab 
# and date of fundation
def get_company_information(driver):
    l = []
    # each company has its own node(id) so for each company we need 
    # to get the proper id
    try :
        id = driver.find_element_by_class_name('node').get_attribute('id')
    except :
        return l

    #company name
    try :
        company_name = driver.find_element_by_xpath('//*[@id="squeeze"]/h2').text
        l.append(company_name)
    except :
        l.append(None)

    #company e-mail
    try :
        company_email = driver.find_element_by_xpath('//*[@id="' + str(id) + '"]/div[1]/div[2]/div[1]/div/a[1]').text
        l.append(company_email)
    except :
        l.append(None)
    
    #company phone
    try:
        company_phone = driver.find_element_by_xpath('//*[@id="' + str(id) + '"]/div[1]/div[2]/div[1]/div/a[2]').text 
        l.append(company_phone)
    except :
        l.append(None)
    

    #place number + company collab + fundation date
    place_nb = True
    try :
        company_date = driver.find_element_by_xpath('//*[@id="' + str(id) + '"]/div[1]/div[2]/div[1]/div/p[3]/strong').text
    except:
        place_nb = False


    if (place_nb == True):
        try :
        #company place number
            company_collab = driver.find_element_by_xpath('//*[@id="' + str(id) + '"]/div[1]/div[2]/div[1]/div/p[1]').text        
            l.append(company_collab)
        except :
            l.append(None)

        try :
        #company collabs
            company_collab = driver.find_element_by_xpath('//*[@id="' + str(id) + '"]/div[1]/div[2]/div[1]/div/p[2]').text        
            l.append(company_collab)
        except :
            l.append(None)

        #company fundation date
        try :
            company_date = driver.find_element_by_xpath('//*[@id="' + str(id) + '"]/div[1]/div[2]/div[1]/div/p[3]/strong').text
            l.append(company_date)
        except :
            l.append(None)
    else :
        #place number
        l.append(None)
        #company collabs
        try :
            company_collab = driver.find_element_by_xpath('//*[@id="' + str(id) + '"]/div[1]/div[2]/div[1]/div/p[1]').text        
            l.append(company_collab)
        except :
            l.append(None)

        #company fundation date
        try :
            company_date = driver.find_element_by_xpath('//*[@id="' + str(id) + '"]/div[1]/div[2]/div[1]/div/p[2]/strong').text
            l.append(company_date)
        except :
            l.append(None)
        
    #company incubator/accelerator
    try:
        incubator = driver.find_element_by_xpath('//*[@id="' + str(id) + '"]/div[1]/div[2]/div[1]/div/div/div/h1').text
        l.append(incubator)
    except:
        l.append(None)

    return l

# save information of a company a csv file
def write_list_in_file(driver,files_name,list1):
    with open(files_name, "a") as fp:
        wr = csv.writer(fp, dialect='excel')
        ## getting information of each company
        wr.writerow(list1)
    fp.close()


######################################################################
######################## FOR EMPLOYEES ###############################

def get_employees_profile_links(driver):
    users_links = []
    i = 1
    search = True
    #in this while loop, we'll search for each employee's url
    while search :
        try :
            elem = driver.find_element_by_xpath('//*[@id="list-utilisateurs-inner"]/div['+str(i)+']/a').get_attribute('href')
            users_links.append(elem)
            i += 1
        except :
            search = False
    return users_links

def get_employees_info(driver,employees_profiles) :
    employees_info= []
    for elem in employees_profiles :
        driver.get(elem)
        driver.implicitly_wait(5)
        employee_info = []

        #company name 
        try : 
            employee_company = driver.find_element_by_xpath('//*[@id="content-profil-items-profil"]/div[3]/div[1]/div[2]/a/h1').text
            employee_info.append(employee_company)
        except :
            employee_company = None
            employee_info.append(employee_company)
        #name
        try :
            employee_name = driver.find_element_by_xpath('//*[@id="content-profil-items-profil"]/div[2]/h1').text
            employee_info.append(employee_name)
        except :
            employee_name = None
            employee_info.append(employee_name)            
        #phone number
        try :
            employee_phone = driver.find_element_by_xpath('//*[@id="content-profil-items-profil"]/div[2]/a[1]').text
            employee_info.append(employee_phone)
        except:
            employee_phone = None
            employee_info.append(employee_phone)
        #email
        try :
            employee_email = driver.find_element_by_xpath('//*[@id="content-profil-items-profil"]/div[2]/a[2]').text
            employee_info.append(employee_email)
        except :
            employee_email = None
            employee_info.append(employee_email)

        #employee position
        try :
            employee_position = driver.find_element_by_xpath('//*[@id="content-profil-items-profil"]/div[3]/div[1]/div[2]/div[1]/p[2]').text
            employee_info.append(employee_position)
        except :
            employee_position = None
            employee_info.append(employee_position)
        # append employees list
        employees_info.append(employee_info)
    return employees_info

def write_employees_info_in_file(driver, employees_info,files_name):
    with open(files_name, "a") as fp:
        wr = csv.writer(fp, dialect='excel')
        ## getting information of each company
        for elem in employees_info :
            wr.writerow(elem)
    fp.close()
    


##########################################################################
################################ MAIN ####################################

def main():
    ##### part 1 : getting all companies urls
    
    driver = browse('https://pepiniere27.synintra.com/')
    connect(driver,'hicham.oumouss@breakpoint-technology.fr','issou78440')
    
    #go into local network
    get_local_network(driver)

    #loading page (get more companies)
    more_load(driver)
    
    #getting companies urls
    companies_urls = get_companies_urls(driver)
    time.sleep(3)

    write_list_in_file(driver,'companies_urls.csv',companies_urls)

    ##### part 2 : getting all companies & employees information
    for company_url in companies_urls :
        try :
            driver.get('https://pepiniere27.synintra.com' + str(company_url))
            driver.implicitly_wait(5)
            #getting one company information
            company_info = get_company_information(driver)

            #getting company information in the companies.csv file
            write_list_in_file(driver,'companies.csv',company_info)

            # getting all emlpoyees information
            #click on users list
            driver.find_element_by_xpath('//*[@id="li-list-utilisateurs"]/a').click()
            driver.implicitly_wait(5)

            # get employees profile links
            employees_profiles = get_employees_profile_links(driver)
            # get employees information 
            employees_info = get_employees_info(driver,employees_profiles)
            #save employees information
            write_employees_info_in_file(driver,employees_info,'employees.csv')
            print('Save')
        except:
            print('Pass')
      
    print('The end :D !')
    time.sleep(3)
    driver.quit()
    return companies_urls

main()


